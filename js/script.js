"use strict";

function createNewUser() {
  this.firstName = prompt("Enter you first name: ", "");

  this.lastName = prompt("Enter you last name", "");

  this.getLogin = function () {
    let newLogin =
      this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    return newLogin;
  };
}

let newUserObj = new createNewUser();
console.log(`Your login is: ${newUserObj.getLogin()}`);
